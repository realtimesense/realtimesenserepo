import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-prediction',
  templateUrl: './prediction.component.html',
  styleUrls: ['./prediction.component.scss']
})
export class PredictionComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  addAPrediction(): void {
    this.router.navigate(["/prediction/adding"])
  }

  goToLanding(): void {
    this.router.navigate(["/prediction/landing"])
  }
}

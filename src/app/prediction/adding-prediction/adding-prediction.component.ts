import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-adding-prediction',
  templateUrl: './adding-prediction.component.html',
  styleUrls: ['./adding-prediction.component.scss']
})
export class AddingPredictionComponent implements OnInit {
  newPredictionName: String

  constructor(private router: Router) { }

  ngOnInit() {
  }

  goBackToPrediction(): void {
    this.router.navigate(['/prediction'])
  }

}

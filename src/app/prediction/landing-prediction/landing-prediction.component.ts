import { Component, OnInit } from '@angular/core';
import * as d3 from 'd3';

@Component({
  selector: 'app-landing-prediction',
  templateUrl: './landing-prediction.component.html',
  styleUrls: ['./landing-prediction.component.scss']
})
export class LandingPredictionComponent implements OnInit {
  autoTicks = false;
  max = 100;
  min = 0;
  showTicks = true;
  step = 1;
  thumbLabel = true;
  disable = false;

  constructor() {

    // // Set the dimensions of the canvas / graph
    // var margin = { top: 30, right: 20, bottom: 30, left: 50 },
    //   width = 100 - margin.left - margin.right,
    //   height = 50 - margin.top - margin.bottom;

    // // Set the ranges
    // var x = d3.time.scale().range([0, 100]);
    // var y = d3.scale.linear().range([50, 0]);

    // // define the line
    // let line = d3.svg.line()
    //   .x(function (d) { return x(d.time); })
    //   .y(function (d) { return y(d.amount); });

    // // initilize and add svg canvas 
    // let svg = d3.select("p").style("color", "white");
  }

  ngOnInit() {
  }

  get tickInterval(): number | 'auto' {
    return this.showTicks ? (this.autoTicks ? 'auto' : this._tickInterval) : 0;
  }
  set tickInterval(v) {
    this._tickInterval = Number(v);
  }
  private _tickInterval = 1;

}

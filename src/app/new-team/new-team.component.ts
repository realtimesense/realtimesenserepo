import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-new-team',
  templateUrl: './new-team.component.html',
  styleUrls: ['./new-team.component.scss']
})
export class NewTeamComponent implements OnInit {
  private newTeamName: String

  constructor(public dialogRef: MatDialogRef<NewTeamComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {

  }

  onButtonsClick(switsh: boolean): void {
    if (!switsh) {
      console.log(this.data);
      this.data.push({ name: this.newTeamName, id: '4' });
    }
    this.dialogRef.close();
  }

}

import { DatabaseService } from './services/database.service';
import { LandingPredictionComponent } from './prediction/landing-prediction/landing-prediction.component';
import { AddingPredictionComponent } from './prediction/adding-prediction/adding-prediction.component';
import { PredictionComponent } from './prediction/prediction.component';
import { TeamComponent } from './team/team.component';
import { NewTeamComponent } from './new-team/new-team.component';
import { RoutingModule } from './app-routing.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AuthGuardService } from './services/auth-guard.services';
import { AuthService } from './services/auth.service';
import { HttpClientModule } from '@angular/common/http';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

import { environment } from '../environments/environment';

import { AppComponent } from './app.component';
import { TopBarComponent } from './top-bar/top-bar.component';
import { TokenInterceptor } from './services/token.interceptor';

import {
  MatAutocompleteModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatDialogModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule
} from '@angular/material';

import 'hammerjs';
import { ProjectDisplayPageComponent } from './project-display-page/project-display-page.component';
import { HomeDisplayPageComponent } from './home-display-page/home-display-page.component';
import { DeviceDisplayComponent } from './project-display-page/device-display/device-display.component';
import { ProjectLandingComponent } from './project-display-page/project-landing/project-landing.component';
import { DeviceAddComponent } from './device-add/device-add.component';
import { StartRunModal } from './start-run-modal/start-run-modal.component';
import { StopRunModal } from './stop-run-modal/stop-run-modal.component';
import { LoginPageComponent } from './login-page/login-page.component';
import { LandingDisplayPageComponent } from './landing-display-page/landing-display-page.component';
import { FormsModule } from '@angular/forms';
// import { AddingPredictionComponent } from './adding-prediction/adding-prediction.component';
// import { LandingPredictionComponent } from './landing-prediction/landing-prediction.component';
import { CompetitionDisplayPageComponent } from './competition-display-page/competition-display-page.component';

export const firebaseConfig = {
  apiKey: "AIzaSyCcmjZ850SIlSLgi7A7XqnyDtVpfI-qrH0",
  authDomain: "realtimesense.firebaseapp.com",
  databaseURL: "https://realtimesense.firebaseio.com",
  projectId: "realtimesense",
  storageBucket: "realtimesense.appspot.com",
  messagingSenderId: "611303353097"
};
export const MaterialModules = [
  MatAutocompleteModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatDialogModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
  FormsModule
];

@NgModule({
  declarations: [
    AppComponent,
    TopBarComponent,
    ProjectDisplayPageComponent,
    HomeDisplayPageComponent,
    DeviceDisplayComponent,
    ProjectLandingComponent,
    DeviceAddComponent,
    StartRunModal,
    StopRunModal,
    LoginPageComponent,
    LandingDisplayPageComponent,
    TeamComponent,
    PredictionComponent,
    NewTeamComponent,
    AddingPredictionComponent,
    LandingPredictionComponent,
    CompetitionDisplayPageComponent
  ],
  entryComponents: [
    DeviceAddComponent,
    StartRunModal,
    StopRunModal,
    NewTeamComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModules,
    FlexLayoutModule,
    RoutingModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    AngularFireDatabaseModule,
    HttpClientModule
  ],
  providers: [AuthService, AuthGuardService, DatabaseService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    }],
  bootstrap: [AppComponent]
})
export class AppModule { }
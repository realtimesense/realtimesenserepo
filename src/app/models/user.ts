import { AngularFireSnapshot } from './angularfire-snapshot';
export enum UserRole {
    NOT_INITIALIZED = -1,
    NOT_SIGNED_IN = 0,
    USER = 1,
    TEAM_MEMBER = 2,
    ADMIN = 3,
}
  
export interface UserInfo {
    userRole: UserRole;
    uid?: string;
    name?: string;
    email?: string;
    photoUrl?: string;
}
  
export class User implements AngularFireSnapshot{
    [propName: string]: any;
    $key: string;
    $exists(): boolean {
        throw new Error("Method not implemented.");
    }
    public uid: string;
    public name: string;
    email?: string;
    public photoUrl: string;

    constructor(obj?: any) {
        this.uid = obj && obj.uid || '';
        this.name = obj && obj.name || '';
        this.email = obj && obj.email || '';
        this.photoUrl = obj && obj.photoUrl || '';
    }
}
  
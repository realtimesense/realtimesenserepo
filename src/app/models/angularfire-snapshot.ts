export interface AngularFireSnapshot {
    $key: string;
    $exists(): boolean;
    // https://www.typescriptlang.org/docs/handbook/interfaces.html#excess-property-checks
    [propName: string]: any;
}

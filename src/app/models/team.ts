export class Team {
    public uid: string;
    public name: string;
    public members: Array<string>;
    public admins: Array<string>;

    constructor(obj?: any) {
        this.uid = obj && obj.uid || '';
        this.name = obj && obj.name || '';
        this.members = obj && obj.members || new Array<string>();
        this.admins = obj && obj.admins || new Array<string>();
    }
}

import { Component, OnInit, Input} from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { StartRunModal } from '../../start-run-modal/start-run-modal.component';
import { StopRunModal } from '../../stop-run-modal/stop-run-modal.component';

@Component({
  selector: 'project-landing',
  templateUrl: './project-landing.component.html',
  styleUrls: ['./project-landing.component.scss']
})
export class ProjectLandingComponent implements OnInit {

  public isRunning: boolean;
  
  constructor(public dialog: MatDialog) { }

  ngOnInit() {
  }
  
  startRun() {
    let dialogRef = this.dialog.open(StartRunModal, {
      height: '350px',
      width: '400px',
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`); // Run Starts!
      this.isRunning = true;      
    });

    // dialogRef.close('New Device Created!');
  }
  stopRun() {
    let dialogRef = this.dialog.open(StopRunModal, {
      height: '350px',
      width: '400px',
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`); // Run Stops!
      this.isRunning = false;
    });
    
    // dialogRef.close('New Device Created!');
  }
}

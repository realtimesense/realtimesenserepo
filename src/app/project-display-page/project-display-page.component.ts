import { Component, OnInit } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { DeviceAddComponent } from '../device-add/device-add.component';
import { StartRunModal } from '../start-run-modal/start-run-modal.component';
import { StopRunModal } from '../stop-run-modal/stop-run-modal.component';

@Component({
  selector: 'project-display-page',
  templateUrl: './project-display-page.component.html',
  styleUrls: ['./project-display-page.component.scss']
})
export class ProjectDisplayPageComponent implements OnInit {
  private showDevices: boolean;
  private showSenses: boolean;
  
  public deviceId: string;

  constructor(public dialog: MatDialog) { }

  ngOnInit() {
    this.showDevices = false;
    this.showSenses = false;
  }
  addDevice() {
    let dialogRef = this.dialog.open(DeviceAddComponent, {
      height: '280px',
      width: '350px',
      data: { id: this.deviceId }
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`); // Device!
    });
    
    // dialogRef.close('New Device Created!');
  }
  toggleDevices() {
    this.showDevices = !this.showDevices;
  }
  toggleSenses() {
    this.showSenses = !this.showSenses;
  }
}

import { User } from './../models/user';
import { DatabaseService } from './database.service';
import { Injectable, Injector } from '@angular/core';
import { AngularFireAuthModule, AngularFireAuth, } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';
import { Router } from "@angular/router";
import { Observable } from "rxjs";
import 'rosefire';
import { environment } from '../../environments/environment.prod';
import * as firebase from 'firebase/app';

@Injectable()
export class AuthService {

    public user: Observable<firebase.User>;
    public uid: string;
    public username: string;
    public token: string;

    constructor(
        private afAuth: AngularFireAuth,
        // private db: DatabaseService,
        private inj: Injector,
        private router: Router) {

        //Subscribe to AuthState
        this.afAuth.authState.subscribe((authState) => {
            if (authState) {
                this.uid = authState.uid;
                let user: User = new User();
                user.uid = authState.uid;
                user.email = authState.email;
                user.name = authState.displayName;
                user.photoUrl = authState.photoURL;
                authState.getToken().then(token => {
                    this.token = token;
                });
                firebase.database().ref(`/users/${this.uid}`).once("value", snapshot => {
                        const uid = snapshot.val();
                        console.log("uid: ", uid);
                        if (uid) {
                            console.log("user exists!");
                        } else {
                            console.log("new user");
                            // TODO: CODE TO GET USERNAME, hardcoded as Kice for now
                            let db = this.inj.get(DatabaseService);
                            db.createUser("Kice");
                        }
                    });
            } else {
                // console.log("AuthService: User is NOT signed in");
            }
        });

        this.user = this.afAuth.authState;
    }

    get isSignedInStream(): Observable<boolean> {
        return this.afAuth.authState.map<firebase.User, boolean>( (authState) => {
            return authState != null;
        });
    }

    getToken(): string {
        return this.token;
    }

    // private updateUserData(): void {
    //     let path = `users/${this.username}`; // Endpoint on firebase
    //     let data = {
    //                  name: this.user.displayName,
    //                  email: this.user.email,
    //                }
    //     this.db.object(path).update(data)
    //     .catch(error => console.log(error));
    // }

    facebookLogin() {
        let provider = new firebase.auth.FacebookAuthProvider();
        firebase.auth().signInWithPopup(provider).then((result) => {
            // This gives you a Facebook Access Token.
            let token = result.credential.accessToken;
            // The signed-in user info.
            console.log(result);
            this.user = result.user;
            this.username = result.user.displayName;
            result.getToken().then(token => {
                this.token = token;
            });
            this.router.navigate(['/landing']);
        });
    }
    googleLogin() {
        let provider = new firebase.auth.GoogleAuthProvider();
        firebase.auth().signInWithPopup(provider).then((result) => {
            // This gives you a Google Access Token.
            let token = result.credential.accessToken;
            // The signed-in user info.
            console.log(result);
            this.user = result.user;
            this.username = result.user.displayName;
            result.getToken().then(token => {
                this.token = token;
            });
            this.router.navigate(['/landing']);
        });
    }
    twitterLogin() {
        let provider = new firebase.auth.TwitterAuthProvider();
        firebase.auth().signInWithPopup(provider).then((result) => {
            // This gives you a Twitter Access Token.
            let token = result.credential.accessToken;
            // The signed-in user info.
            console.log(result);
            this.user = result.user;
            this.username = result.user.displayName;
            result.getToken().then(token => {
                this.token = token;
            });
            this.router.navigate(['/landing']);
        });
    }
    signInWithRosefire(): void {
        Rosefire.signIn(environment.rosefireRegistryToken, (error, rfUser: RosefireUser) => {
            if (error) {
                // User not logged in!
                console.error(error);
                return;
            }
            this.afAuth.auth.signInWithCustomToken(rfUser.token).then((auth) => {
                this.user = auth;
                this.username = auth.uid;
                auth.getToken().then(token => {
                    this.token = token;
                });
                this.router.navigate(['/landing']);
            });
        });
    }

    signOut(): void {
        this.afAuth.auth.signOut();
        this.user = null;
        this.uid = null;
        this.router.navigate(['/home']);
    }
}

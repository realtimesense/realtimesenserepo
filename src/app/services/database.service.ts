import { AngularFireSnapshot } from './../models/angularfire-snapshot';
import { Observable } from 'rxjs';
import { AngularFireDatabase } from 'angularfire2/database';
import { Injectable } from '@angular/core';
import * as firebase from 'firebase/app';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class DatabaseService {

  constructor(private afdb: AngularFireDatabase, private http: HttpClient) {

  }

  public createUser(username: string) {
    this.http.post(`https://us-central1-realtimesense.cloudfunctions.net/app/user`, {"username": username}).subscribe(data => {
      console.log("user", data);
    });
  }

  public getTeams() {
    // this.http.get(`https://us-central1-realtimesense.cloudfunctions.net/app/team`).subscribe(data => {
    //   console.log(data);
    // });
  }
  /**
   * readList
   */
  public readList(path: string) {
    this.afdb.list(path).snapshotChanges().map(changes => {
      console.log(changes);
    });
  }

  /**
   * readObject
   */
  public readObject(path: string) {
    //TODO: read a single object
  }

  /**
   * pushObject
   */
  public pushObject(path: string, item: AngularFireSnapshot): void {
    this.afdb.list(path).push(item);
  }

  /**
   * contain
   */
  public contain(path: string, uid: string): Boolean {
    firebase.database().ref(path).orderByChild("uid").equalTo(uid).once("value", snapshot => {
      const uid = snapshot.val();
      console.log("uid: ", uid);
      if (uid) {
        console.log("user exists!");
        return true;
      }
    });
    return false;
  }

  /**
   * removeObject
   */
  public removeObject(path: string, key: string): void {
    this.afdb.list(path).remove(key);
  }

  /**
   * updateObject
   */
  public updateObject(path: string, key: string, item: AngularFireSnapshot): void {
    this.afdb.list(path).update(key, item);
  }

}

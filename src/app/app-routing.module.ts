import { LandingPredictionComponent } from './prediction/landing-prediction/landing-prediction.component';
import { AddingPredictionComponent } from './prediction/adding-prediction/adding-prediction.component';
import { PredictionComponent } from './prediction/prediction.component';
import { TeamComponent } from './team/team.component';
import { AppComponent } from './app.component';
import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProjectDisplayPageComponent } from './project-display-page/project-display-page.component';
import { CompetitionDisplayPageComponent } from './competition-display-page/competition-display-page.component';
import { HomeDisplayPageComponent } from './home-display-page/home-display-page.component';
import { DeviceDisplayComponent } from './project-display-page/device-display/device-display.component';
import { ProjectLandingComponent } from './project-display-page/project-landing/project-landing.component';
import { LoginPageComponent } from './login-page/login-page.component';
import { LandingDisplayPageComponent } from './landing-display-page/landing-display-page.component';
import { NewTeamComponent } from './new-team/new-team.component';

import { AuthGuardService } from './services/auth-guard.services';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomeDisplayPageComponent },
  { path: 'competition', component: CompetitionDisplayPageComponent },
  { path: 'login', component: LoginPageComponent },
  { 
    path: '',
    canActivate: [AuthGuardService],
    runGuardsAndResolvers: 'always',
    children: 
    [
      { path: 'landing', component: LandingDisplayPageComponent },
      {
        path: 'project', component: ProjectDisplayPageComponent,
        children: [
          { path: '', redirectTo: 'landing', pathMatch: 'full' },
          { path: 'landing', component: ProjectLandingComponent },
          { path: 'Device1', component: DeviceDisplayComponent },
        ]
      },
      { path: 'team/new', component: NewTeamComponent },
      { path: 'team/:teamKey', component: TeamComponent },
      // { path: 'team/:teamKey/edit', component: TeamEditComponent },
      {
        path: 'prediction', component: PredictionComponent,
        children: [
          { path: '', redirectTo: 'landing', pathMatch: 'full' },
          { path: 'adding', component: AddingPredictionComponent },
          { path: 'landing', component: LandingPredictionComponent }
        ]
      },
      { path: '**', redirectTo: '' }
    ]
  }
  // { path: 'prediction', component: PredictionComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { enableTracing: false })],
  exports: [RouterModule]
})
export class RoutingModule { }

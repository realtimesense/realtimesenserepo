import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeDisplayPageComponent } from './home-display-page.component';

describe('HomeDisplayPageComponent', () => {
  let component: HomeDisplayPageComponent;
  let fixture: ComponentFixture<HomeDisplayPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeDisplayPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeDisplayPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

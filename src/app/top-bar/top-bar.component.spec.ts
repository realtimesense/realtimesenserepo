import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuthService } from '../services/auth.service';


import { TopBarComponent } from './top-bar.component';

describe('TopBarComponentNotSignedIn', () => {
  let component: TopBarComponent;
  let fixture: ComponentFixture<TopBarComponent>;

  beforeEach(async(() => {
    class mockAuthService{};
    TestBed.configureTestingModule({
      declarations: [ TopBarComponent ],
      providers: [{provide: AuthService, useClass: mockAuthService}]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TopBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should render Sign In in a h3 tag if no auth.uid is present', async(() => {
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h3').textContent).toContain('Sign In');
  }));
});

describe('TopBarComponentSignedIn', () => {
  let component: TopBarComponent;
  let fixture: ComponentFixture<TopBarComponent>;

  beforeEach(async(() => {
    class mockAuthService{ uid = "Testing" };
    TestBed.configureTestingModule({
      declarations: [ TopBarComponent ],
      providers: [{provide: AuthService, useClass: mockAuthService}]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TopBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should render auth.uid in a h3 tag when auth.uid is present', async(() => {
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h3').textContent).toContain('Testing');
  }));
});

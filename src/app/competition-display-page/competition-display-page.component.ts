import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-competition-display-page',
  templateUrl: './competition-display-page.component.html',
  styleUrls: ['./competition-display-page.component.scss']
})
export class CompetitionDisplayPageComponent implements OnInit {
  tweets = [
    {text: 'One', color: 'lightblue'},
    {text: 'Two', color: 'lightgreen'},
    {text: 'Three', color: 'lightpink'},
    {text: 'Four', color: '#DDBDF1'},
  ];
  
  constructor() { }

  ngOnInit() {
  }

}

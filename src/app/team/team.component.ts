import { NewTeamComponent } from './../new-team/new-team.component';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-team',
  templateUrl: './team.component.html',
  styleUrls: ['./team.component.scss']
})
export class TeamComponent implements OnInit {
  teamName: string;
  projects = [{ name: '2016 Car', id: '1' },
  { name: '2017 Car', id: '2' },
  { name: '2018 Car', id: '3' }]
  

  constructor(private router: Router, public dialog: MatDialog) { }
  

  ngOnInit() { }

  goToDetail(project: String): void {
    console.log(project)
    //TODO: change project to the actual id of the project once setting up the database
    this.router.navigate(["/project/" + project])
  }

  openAddProjectDialog(): void {
    let dialogRef = this.dialog.open(NewTeamComponent, {
      height: '400px',
      width: '600px',
      data: this.projects
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.teamName = result;
    });
  }




}

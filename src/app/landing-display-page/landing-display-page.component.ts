import { Component, OnInit } from '@angular/core';
import { DatabaseService } from './../services/database.service';

@Component({
  selector: 'app-landing-display-page',
  templateUrl: './landing-display-page.component.html',
  styleUrls: ['./landing-display-page.component.scss']
})
export class LandingDisplayPageComponent implements OnInit {

  constructor(private db: DatabaseService) { }

  ngOnInit() {
    this.db.getTeams();
  }

}

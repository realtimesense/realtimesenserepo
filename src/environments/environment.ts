// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyCcmjZ850SIlSLgi7A7XqnyDtVpfI-qrH0",
    authDomain: "realtimesense.firebaseapp.com",
    databaseURL: "https://realtimesense.firebaseio.com",
    projectId: "realtimesense",
    storageBucket: "",
    messagingSenderId: "611303353097"
  },
  rosefireRegistryToken: "11217190-c699-44bd-9bd5-8de3a82cee3d",
};
